﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetTheMiddleCharater;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetTheMiddleCharater.Tests
{
    [TestClass]
    public class KataTests
    {
        [DataRow("test", "es")]
        [DataRow("testing", "t")]
        [DataRow("middle", "dd")]
        [DataRow("A", "A")]
        [DataTestMethod]
        public void GetMiddleTest(string input, string expected)
        {
            Assert.AreEqual(expected, Kata.GetMiddle(input));
        }
    }
}