﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetTheMiddleCharater
{
    public class Kata
    {
        public static string GetMiddle(string input)
        {
            return input.Length < 3 ? input : GetMiddle(input.Substring(1, input.Length - 2));
        }
    }
}
